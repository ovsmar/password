# Password Generator

This project is a simple password generator written in Bash. It allows you to generate a random password with a specified length and character set.

## Usage

You can run the password generator with the following command:

```bash
./password.bash [-l length] [-help] [-copy] [character set flags]
```

### Options

- `-l length`: Set the length of the password (default: 12)
- `-help`: Display the help message and available character sets
- `-copy`: Copy the generated password to the clipboard

### Character Set Flags

You can specify one or more character set flags to determine the types of characters that will be included in the generated password:

- `lc`: Lowercase characters
- `n`: Numbers
- `uc`: Uppercase characters
- `s`: Special characters
- `all`: All character sets (default)

## Examples

Generate a 16-character password with all character sets:
```bash
./password.bash -l 16 all
```

Generate a 12-character password with lowercase characters and numbers, and copy it to the clipboard:
```bash
./password.bash -l 12 lc n -copy
```

## Requirements
This script requires bash to run. If you want to use the -copy option, you will also need xclip installed on Linux or pbcopy on macOS.

## Installation
To install the password generator, you can clone the repository and make the script executable:

```bash
git clone https://gitlab.com/ovsmar/password
cd password-generator
sudo chmod +x password.bash
```

You can then run the script from the command line as shown in the Usage section.

## Alias

1. Open your bash profile file. This is typically located at ~/.bashrc or ~/.bash_profile or ~/.zshrc
2. Add the following line at the end of the file:

```bash
alias gpass='/path/to/gpass.bash'
```

1. Save and close the file.
2. To make the changes take effect, you can either restart your terminal or source your bash profile with the command source ~/.bashrc or source ~/.bash_profile  or ~/.zshrc

Now, you can use the command gpass in your terminal to run the password generator script.

```bash
gpass -l 32 -c

Generated password:

HcEb{XtCds/Ph>},<y4^T24b8O)?rSmC

```
