#!/bin/bash

# Set default values for parameters
LENGTH=12
CHARSET="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789@#$%^&*()_+-={}:<>?"  # Default to all character sets
CHARSETS_FILE="/tmp/charsets_displayed"  # Temporary file to store flag
COPY=false

# Function to print character set explanations
print_character_sets() {

    echo -e "\e[1;34m==========================\e[0m"
    echo -e "\e[1;34mAvailable character sets:\e[0m"
    echo -e "\e[1;32mlc - Lowercase characters\e[0m"
    echo -e "\e[1;32mn - Numbers\e[0m"
    echo -e "\e[1;32muc - Uppercase characters\e[0m"
    echo -e "\e[1;32ms - Special characters\e[0m"
    echo -e "\e[1;32mall - All character sets\e[0m"
    echo -e "\e[1;34m==========================\e[0m"
    echo ""
    touch "$CHARSETS_FILE"  # Create/update the temporary file
}

# Function to print usage
print_usage() {
    echo ""
    echo -e "\e[1;34mUsage: $0 [-l length] [-help] [-copy] [character set flags]\e[0m"
    echo -e "\e[1;32m  -l length    Set the length of the password (default: 12)\e[0m"
    echo -e "\e[1;32m  -help        Display this help message and available character sets\e[0m"
    echo -e "\e[1;32m  -copy        Copy the generated password to the clipboard\e[0m"
    echo ""
    echo -e "\e[1;34mCharacter set flags:\e[0m"
    print_character_sets
}

# Function to calculate password strength
password_strength() {
    local PASSWORD=$1
    local STRENGTH="Weak 💔"
    local COLOR="\e[1;31m"  # Red color for weak password

    local LENGTH_SCORE=$((${#PASSWORD} / 4))  # Divide length by 4 for scoring
    local UPPERCASE=$(echo $PASSWORD | grep -o '[A-Z]' | wc -l)
    local LOWERCASE=$(echo $PASSWORD | grep -o '[a-z]' | wc -l)
    local NUMBER=$(echo $PASSWORD | grep -o '[0-9]' | wc -l)
    local SPECIAL=$(echo $PASSWORD | grep -o '[@#$%^&*()_+-={}:<>?]' | wc -l)

    local TOTAL_SCORE=$((LENGTH_SCORE + UPPERCASE + LOWERCASE + NUMBER + SPECIAL))

    if (( TOTAL_SCORE > 15 )); then
        STRENGTH="Strong 💪"
        COLOR="\e[1;32m"  # Green color for strong password
    elif (( TOTAL_SCORE > 10 )); then
        STRENGTH="Medium 😐"
        COLOR="\e[1;33m"  # Yellow color for medium password
    fi

    echo -e "Password strength: $COLOR$STRENGTH\e[0m"
}

# Parse command-line arguments
while getopts ":l:hc" opt; do
    case $opt in
        l) LENGTH="$OPTARG";;
        h) print_usage; exit 0;;
        c) COPY=true;;
        \?) echo "Invalid option: -$OPTARG"; exit 1;;
    esac
done
shift $((OPTIND-1))

# Check if character sets have been displayed or help requested
if [ ! -f "$CHARSETS_FILE" ] && [ "$1" != "-help" ]; then
    print_character_sets
fi

# Process flags
while [ $# -gt 0 ]; do
    case $1 in
        lc) CHARSET="abcdefghijklmnopqrstuvwxyz";;
        n) CHARSET="0123456789";;
        uc) CHARSET="ABCDEFGHIJKLMNOPQRSTUVWXYZ";;
        s) CHARSET="@#$%^&*()_+-={}:<>?";;
        all) CHARSET="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789@#$%^&*()_+-={}:<>?";;
        -copy) COPY=true;;
        *) echo "Invalid flag: $1"; exit 1;;
    esac
    shift
done

# Generate the password
if [ -z "$LENGTH" ] || ! [[ "$LENGTH" =~ ^[0-9]+$ ]]; then
    echo "Error: Invalid password length"
    exit 1
fi
PASSWORD=$(tr -dc "$CHARSET" < /dev/urandom | fold -w $LENGTH | head -n 1)

# Print the password and copy to clipboard if requested
echo -e "\n\e[1;34mGenerated password:\e[0m"
echo -e "\e[1;32m$PASSWORD\e[0m\n"

# Check password strength
password_strength $PASSWORD

if [ "$COPY" = true ]; then
    if command -v xclip >/dev/null 2>&1; then
        echo -n "$PASSWORD" | xclip -selection clipboard  # Copy to clipboard (Linux)
    elif command -v pbcopy >/dev/null 2>&1; then
        echo -n "$PASSWORD" | pbcopy  # Copy to clipboard (macOS)
    else
        echo "Error: Neither xclip nor pbcopy installed. Cannot copy to clipboard."
    fi
fi